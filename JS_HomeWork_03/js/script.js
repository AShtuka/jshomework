function inputValues(message = "Hi, please enter your number", oldValue  = "") {
    firstNum = prompt(message, oldValue);
    secondNum = prompt(message, oldValue);

    if(!(!isNaN(parseFloat(firstNum)) && isFinite(firstNum)) || !(!isNaN(parseFloat(secondNum)) && isFinite(secondNum)) ) {
        inputValues("Sorry, but this isn't number",
            "As first number you input - " + firstNum + " As second number you input - " + secondNum);
    }
    return {
        firstNum: +firstNum,
        secondNum: +secondNum
    }
}

function inputOperation(oldValue = "") {
    let operationSymbol = prompt(`Please choose symbol of operation :
                                                +  for addition
                                                -  for subtraction
                                                /  for division
                                                *  for multiplication`, oldValue);
    let expr  = /[-+*/]/g;
    if (operationSymbol.length > 1 || operationSymbol.search(expr)){
        operationSymbol = inputOperation(operationSymbol);
    }
    return operationSymbol;
}


function calc(operationSymbol, firstNum, secondNum) {
    switch (operationSymbol) {
        case "+" : {
            alert("The result is " + addition(firstNum, secondNum));
            break;
        }
        case "-" : {
            alert("The result is " + subtraction(firstNum, secondNum));
            break;
        }
        case "*" : {
            alert("The result is " + multiplication(firstNum, secondNum));
            break;
        }
        case "/" : {
            alert("The result is " + division(firstNum, secondNum));
            break;
        }
    }
}

function addition(fistNum, secondNum) {
    return fistNum + secondNum;
}

function subtraction(fistNum, secondNum) {
    return fistNum - secondNum;
}

function multiplication(fistNum, secondNum) {
    return fistNum * secondNum;
}

function division(fistNum, secondNum) {
    return fistNum / secondNum;
}

let breakPoint = true;
while (breakPoint) {
    let values = inputValues();
    let symbol = inputOperation();
    calc(symbol, values.firstNum, values.secondNum);
    breakPoint = confirm("Are you wish continue?");
}
