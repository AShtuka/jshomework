function obtainArr() {
    const table = document.querySelectorAll('table');
    let arr = table[0].childNodes[3].childNodes;
    arr = Array.from(arr);
    arr = arr.filter((item) => {
        if (item.nodeName === "#text" || item.className === "") {
            return false;
        } else {
            return true;
        }
    })
    return arr;
}
// arr.map((item) => {
//     if (item.innerText !== "Russian" &&
//         item.innerText !== "Spanish" &&
//         item.innerText !== "French") {
//         item.style.color = "#DCECF8";
//     }
// })
// //
const keyWordsFilter = (arr, keyWords) => {
    arr.map((item) => {
        if (item.innerText !== "Russian" &&
            item.innerText !== "Spanish" &&
            item.innerText !== "French") {
            item.style.color = "#DCECF8";
        }
    })
}

const parseFieldName = (fieldNames) => {
    let arrWithKitchenName = [];
    let arrWithFoodsCategory = [];
    if (fieldNames.length !== 0) {
        fieldNames.forEach((item) => {
            if (item.className === "russian" ||
                item.className === "spanish" ||
                item.className === "french") {
                arrWithKitchenName.push(item);
            } else {
                arrWithFoodsCategory.push(item);
            }
        })
    }
    return [arrWithKitchenName, arrWithFoodsCategory];
}

const filterForKitchen = (arrWithKitchenName, arrForFilter) => {
    const result = [];
    arrWithKitchenName.map((kitchenName) => {
        arrForFilter.filter((item) => {
            if (item.className === kitchenName){
                result.push(item);
            };
        })
    })
    return result;
}

function filterCollection(arrForFilter, keyWords, flagForKey, ...fieldNames) {
    let result = [];
    let arrOfKeyWords = keyWords.split(" ");
    let arrWithFieldNames = parseFieldName(fieldNames);
    let arrWithKitchenName = arrWithFieldNames[0];
    let arrWithFoodsCategory = arrWithFieldNames[1];
    if (arrWithKitchenName.length !== 0){
        result = filterForKitchen(arrWithKitchenName, arrForFilter);
    }
}

let arr = obtainArr();
console.log(arr);
const arrKitch = filterForKitchen(["russian", "french"], obtainArr());
console.log(arrKitch);

