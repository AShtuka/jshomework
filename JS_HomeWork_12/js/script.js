// const countDown = () => {
//     let sec = 5;
//     let timer = document.getElementById("timer");
//     let counter = setInterval(printNumbersTimeout, 1000);
//     function printNumbersTimeout() {
//         let ms = 100;
//         setTimeout(function go() {
//             timer.innerText = `${sec} : ${ms}`;
//             if (ms > 0) {
//                 setTimeout(go, 1);
//             }
//             ms--;
//         }, 0);
//         --sec;
//         if (sec === 0) {
//             clearInterval(counter);
//         }
//     }
// }

const parent = document.querySelector(".images-wrapper");

const changeImg = () => {
    // countDown();
    let firstImg = parent.children[0];
    firstImg.classList.remove("image-to-show");
    firstImg.classList.remove("del-fix-img");
    let imgToRemove = firstImg;
    parent.removeChild(firstImg);
    firstImg = parent.children[0];
    firstImg.classList.add("image-to-show");
    parent.appendChild(imgToRemove);
}

let timerId = setInterval(changeImg, 5000);

const stop = () => {
    clearInterval(timerId);
    const fixImg = document.querySelector(".image-to-show");
    fixImg.classList.remove("image-to-show");
    fixImg.classList.add("fix-img");
}

const start = () => {
    const reStartImg = document.querySelector(".fix-img");
    reStartImg.classList.remove("fix-img");
    reStartImg.classList.add("del-fix-img");
    timerId = setInterval(changeImg, 5000);
}

const stopShow = document.querySelector(".start");
const startShow = document.querySelector(".stop");

stopShow.addEventListener("click", stop);
startShow.addEventListener("click", start);

