const createList = (arr) => {
    let numInList = 1;
    let result = arr.map(function (item, index) {
        if (!Array.isArray(item)) {
            return createStrOfList(item, numInList++);
        } else {
            let tmp = createList(item);
            return createSubList(tmp, index);
        }
    })
    return result;
};

const createStrOfList = (item, numInList) => {
    return `${numInList}. ${item}\n`
}

const createSubList = (arr, index) => {
    let result = arr.map(function (item) {
        if (!Array.isArray(item)) {
            return `${index}.${item}`;
        } else {
            return createSubList(item,index);
        }
    })
    return result;
}

const transformList = (arr) => {
    let arrForChange = arr.slice(3);
    let result = arrForChange.map(function (item) {
        if (Array.isArray(item)) {
            return changeFirstIndex(item);
        } else {
            return item;
        }
    })
    return arr.slice(0, 3).concat(result);
}

const changeFirstIndex = (arrForChange) => {
    let result = arrForChange.map(function (item) {
        if (Array.isArray(item)) {
            return changeFirstIndex(item);
        } else {
            let tmp = +item[0];
            tmp--;
            return tmp + item.substr(1);
        }
    })
    return result;
}

const showList = (arr, paddingLeft = 10) => {
    let padding = paddingLeft;
    let container = document.getElementById("list")
    arr.forEach(function (item) {
        if (!Array.isArray(item)) {
            let listNode = document.createElement("span");
            listNode.innerText = item;
            listNode.style.paddingLeft = `${padding}px`;
            container.appendChild(listNode);
        } else {
            showList(item, paddingLeft += 10);
        }
    })
    return container;
}

let arrCity = ['Rivne',['Volodimerec',['Antonivka', 'Gorodec', 'Svariny'], 'Sarni', 'Varash'], 'Ternopil', ['Volodimerec',['Antonivka','Gorodec', ['Antonivka', 'Gorodec', 'Svariny'], 'Svariny'], 'Sarni', 'Varash'], 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

let list = createList(arrCity);
let resultList = transformList(list);
let container = showList(resultList);

let timer = document.createElement("div");
timer.classList.add("timer");
document.body.appendChild(timer);
let sec = document.createElement("div");
sec.classList.add("sec");
timer.appendChild(sec);

function printNumbersTimeout() {
    let i = 10;
    let timerId = setTimeout(function go() {
        sec.innerText = `${i}`;
        if (i > 0) setTimeout(go, 1000);
        i--;
    }, 0);
}

printNumbersTimeout();

setTimeout(function() {
    sec.remove();
    container.remove();
}, 10000);

