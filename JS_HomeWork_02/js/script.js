function inputInteger(message = "Hi, please enter your number", value = "") {
    let number = prompt(message,value );

    if(!(!isNaN(parseFloat(number)) && isFinite(number)) || (number - Math.round(number) != 0) ) {
        number = inputInteger("Sorry, but this isn't number or your number isn't integer", number);
    }
    return +number;
}

function searchMultiplesOf5(number) {
    if (number < 5 && number > -5) {
        alert("Sorry, no numbers")
    }else if (number > 0 ){
        for (let i = 0 ; i <= number ; i+=5) {
            console.log(i);
        }
    } else {
        for (let i = 0 ; i >= number ; i-=5) {
            console.log(i);
        }
    }
}

function inputRange() {
    let m = inputInteger();
    let n = inputInteger();
    let range = {
        start: m,
        end: n
    }
    if (m >= n) {
        alert("Your first number is " + m + " , and second one is " + n + ". Sorry, but second number must be larger than first");
        range = inputRange();
    }
    return range;
}

function isPrimeNum(number) {
    if (number < 3) {
        return false;
    }
    for (let i = 2; i < number; i++) {
       if(number % i === 0) {
            return false;
       }
    }
    return true;
}

function searchPrimeNumbers(range) {

    let arr = new Array();
    let startOfRange = range.start;
    let end = range.end;

    if (end < 2) {
        alert("Your range doesn't have any prime numbers")
        return;
    }

    if (startOfRange <= 2){
        arr.push(2);
    }

    while(startOfRange <= end) {
        if (isPrimeNum(startOfRange)){
            arr.push(startOfRange);
        }
        startOfRange++;
    }
    return arr;
}

console.log(searchPrimeNumbers(inputRange()));
// searchMultiplesOf5(inputInteger());