// const storyPointArr = [10, 15, 7, 9, 12, 24, 6];
const storyPointArr = [5, 5];
// const backLogArr = [489, 602, 1258, 420, 78, 350];
const backLogArr = [10, 10, 10, 10, 10];

const deadLine = () => new Date(prompt("yyyy.mm.dd", "2019.12.31"));

const  countPoints = (pointArr) => {
    let  points = 0;
    pointArr.forEach(function (item) {
        points += item;
    })
    return points;
}

const countWeekendDays = (workDays) => {
    let currentDay = +(new Date().getDay());
    let daysToWeekend = 6 - currentDay;
    if (workDays >= 7) {
        return Math.floor(workDays/7)*2;
    } else if (workDays <= daysToWeekend) {
        return 0;
    } else if (currentDay === 0) {
        return 1 + workDays;
    } else {
        return 2 + workDays;
    }
}

const searchFinishDate = (dailyPoint, allPoint) => {
    let workDays = allPoint/dailyPoint;
    let weekendDays = countWeekendDays(workDays);
    let workDaysWithWeekend = workDays + weekendDays;
    let finishDate = new Date(new Date().setDate(workDaysWithWeekend));
    return finishDate;
}

const timing = (storyPointArr, backLogArr, deadLine) => {
    let finishDate = searchFinishDate(countPoints(storyPointArr), countPoints(backLogArr));
    if (deadLine > finishDate) {
        let restDays = Math.ceil(Math.abs(deadLine.getTime() - finishDate) / (1000 * 3600 * 24));
        console.log("Все задачи будут успешно выполнены за " + restDays +  " дней до наступления дедлайна!")
    } else {
        let needHours = Math.ceil(Math.abs(finishDate - deadLine.getTime()) / (1000 * 3600));
        console.log("Команде разработчиков придется потратить дополнительно " + needHours + " часов после дедлайна, чтобы выполнить все задачи в беклоге");
    }
}

timing(storyPointArr, backLogArr, deadLine());


