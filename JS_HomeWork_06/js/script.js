function filterBy(arr, dataType) {
    return arr.filter(function (item) {
        return (typeof item !== dataType)
    })
}

let arr = ['hello', 'world', 23, '23', null, true, false, {}];
let newArr = filterBy(arr, "object");
console.log(newArr);
