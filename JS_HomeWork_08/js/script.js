//____________Create-Elements_____________

const spnWithPrice = document.createElement("span");
spnWithPrice.classList.add("spn");

const notValidData = document.createElement("p");
notValidData.classList.add("not-valid-data");
notValidData.innerText = "Please enter correct price!!!";
notValidData.hidden = true;
document.querySelector(".container").appendChild(notValidData);

const createSpnClose = () => {
    const spnClose = document.createElement("span");
    spnClose.classList.add("close");
    return spnClose;
}


const inputField = document.querySelector(".input-field");
const spnWr = document.querySelector(".spn-wr");


inputField.addEventListener("focus", (event) => {
    inputField.classList.remove("red-border");
    inputField.classList.remove("green-font");
    inputField.classList.add("green-border");
    notValidData.hidden = true;

})

inputField.addEventListener("blur", () => {
    inputField.classList.remove("green-border");
    if((!isNaN(parseFloat(inputField.value)) && isFinite(inputField.value)) && inputField.value > 0 ){
        inputField.classList.add("green-font");
        spnWithPrice.innerHTML = "Текущая цена: " + inputField.value;
        spnWithPrice.appendChild(createSpnClose());
        document.querySelector(".spn-wr").appendChild(spnWithPrice);
    } else {
        inputField.classList.add("red-border");
        notValidData.hidden = false;
    }
})

spnWr.addEventListener("click", () => {
    inputField.value = "";
    document.querySelector(".spn").remove();
})


