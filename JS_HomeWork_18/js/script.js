const askInfo = (request = "Hi!", sampleFill = "someInfo") => prompt(request, sampleFill);

const askGrade = (message = "Enter grade by ", subject = "Math", grade = "11") => {
    grade = prompt(message + subject, grade);
    if (grade === null) return grade;
    if(!(!isNaN(parseFloat(grade)) && isFinite(grade)) || (grade - Math.round(grade) != 0) || grade < 0 || grade > 12 ) {
        grade = askGrade("Sorry you enter wrong grade by ", subject , grade);
    }
    return +grade;
}

const fillTabel = (student) => {
    let subjectName;
    let grade;
    while (true){
        subjectName = askInfo("Enter subject name", "Math for example");
        grade = askGrade();
        if (subjectName !== null || grade !== null) {
            student.addField(subjectName, grade);
        } else {return}
    }
}

const createNewStudent = () => {
    const student = {};
    student.firstName = askInfo("Input your name, please", "Ivan for example");
    student.lastName = askInfo("Input your surname, please", "Ivaniv for example");
    student.tabel = {};

    student.addField = function (fieldName,fieldValue) {
        this.tabel[fieldName] = fieldValue;
    };

    student.academicPerformance = function () {
        let amongBadGrade = 0;
        let gradePointAverage = 0;
        let amongOfSubject = 0;
        for (key in this.tabel) {  // Про this Микита в аудиторії розповідав
            if (this.tabel[key] < 4) {
                amongBadGrade++;
            }
            gradePointAverage += this.tabel[key];
            amongOfSubject++;
        }
        if (amongBadGrade !== 0) {
            return "You have bad grade by " + amongBadGrade + " subjects";
        } else if (gradePointAverage/amongOfSubject > 7 ) {
            return "Студенту назначена стипендия";
        } else {
            return "Студент переведен на следующий курс";
        }
    }
    return student;
}

let student = createNewStudent();
fillTabel(student);
console.log(student);
console.log(student.academicPerformance());
