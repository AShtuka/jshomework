const $btnToTop = $(".btn-to-top");

$(window).scroll(function () {
    if ($(window).scrollTop() >= $(window).height()){
        $btnToTop.fadeIn();
    } else {
        $btnToTop.fadeOut();
    }
})

$btnToTop.click(function () {
    $("html,body").animate({scrollTop:0}, 900);
})

$(".submenu-item").click(function (event) {
    event.preventDefault();
    let scrollTo = "#" + $(this).attr('data-name');
    $('html, body').animate({
        scrollTop: $(scrollTo).offset().top
    }, 900);
})

const $topRatedBtn = $(".top-rated-btn");
$topRatedBtn.click(function () {
    $(".top-rated-wr").slideToggle("uncover");
    $(".top-rated-btn i").toggle("uncover");
})