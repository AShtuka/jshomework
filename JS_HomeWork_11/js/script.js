document.addEventListener("keyup", (event) => {
    const btnList = document.querySelector(".btn-wrapper").children;
    for (let i = 0; i < btnList.length; i++) {
        if (btnList[i].innerText.toLowerCase() === event.key.toLowerCase()) {
            btnList[i].style.backgroundColor = "blue";
        } else {
            btnList[i].style.backgroundColor = "#33333a";
        }
    } 
})