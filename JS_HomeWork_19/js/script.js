const objForClone = {
    num: 19,
    str: "Artem",
    bool: true,
    obj: {
        num: 19,
        str: "Artem",
        bool: true,
        arr: [10, 15, 7, {},{num: 19, str: "Artem", bool: true, arr:[10, 15, 7], getName: function () {}}, null],
        getName: function () {}
    },
    arr: [10, 15, 7, {},{num: 19, str: "Artem", bool: true, arr:[10, 15, 7], getName: function () {}}],
    getName: function (name, surnane) {
        return name + "Hi" + surnane;
    },
    null: null
}

function cloneArr(arr) {
   const arrClone = arr.map(function (item) {
        if (item === null) {
            return null;
        } else if (typeof item === "object") {
            return objClone(item);
        } else {
            return item;
        }
    })
    return arrClone;
}

function objClone (objForClone) {
    const newObj = {};
    for (key in objForClone) {

        if (objForClone[key] === null){
            newObj[key] = null;
        }
        else if (Array.isArray(objForClone[key])){
            newObj[key] = cloneArr(objForClone[key]);
        }
        else if (typeof objForClone[key] === "object") {
            newObj[key] = objClone(objForClone[key]);
        }
        else {
            newObj[key] = objForClone[key];
        }
    }
    return newObj;
}

let obj = objClone(objForClone);
console.log(obj);
console.log(objForClone);



