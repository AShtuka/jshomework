const btn = $(".btn");

const deleteCircle = () => {
  $(".circle-item").click(function () {
    $(this).hide(500);
    // const  item = $(this);
    // setTimeout(function () {
    //   item.remove();
    //   if ($(".container").children().length === 0) {
    //     $(".container").remove();
    //     $("body").prepend(btn);
    //     btn.text("Нарисовать круг");
    //     btn.one("click", createInputField);
    //   }
    // },500);
  })
}

const randomInteger = (min, max) => {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  rand = Math.round(rand);
  return rand;
}

const createColor = () => {
  return `rgb(${randomInteger(0, 255)},${randomInteger(0, 255)}, ${randomInteger(0, 255)})`
}

const createCircles = (circleD) => {
  const container = $("<div></div>");
  container.addClass("container");
  container.css("width", `${circleD * 10 + 40}px`);
  for (let i = 0; i < 100; i++) {
    const circle = $("<div></div>");
    circle.addClass("circle-item");
    circle.css({
      width : circleD + "px",
      height : circleD + "px",
      backgroundColor : createColor()
    })
    container.append(circle);
  }
  return container;
}

const drawCircle = () => {
  const circleD = $(".input-field").val();
  $(".input-field").val("");
  $(".input-wr").remove();
  btn.detach();
  const container = createCircles(circleD);
  $("body").prepend(container);
  deleteCircle();
}

const dataValidator = () => {
  const inputField = $(".input-field");
  inputField.focus(function () {
    inputField.removeClass("red-border");
    inputField.addClass("green-border");
  })

  inputField.blur(function () {
    inputField.removeClass("green-border");
    if((!isNaN(parseFloat(inputField.val())) && isFinite(inputField.val())) && inputField.val() > 0 ){
        btn.text("Нарисовать");
        btn.on("click", drawCircle);
    } else {
      inputField.removeClass("red-border");
      inputField.addClass("red-border");
      inputField.val("");
      inputField.attr("placeholder", "Введите коректные данные");
    }
  })
}

const createInputField = () => {
  const inputField = $("<input>");
  inputField.attr("type", "text");
  inputField.addClass("input-field");
  const inputFieldWr = $("<p></p>");
  inputFieldWr.text("Введите диаметр круга : ")
  inputFieldWr.addClass("input-wr");
  inputFieldWr.append(inputField);
  inputFieldWr.insertAfter(btn);
  console.log("ura");
  dataValidator();
}

btn.one("click", createInputField);


