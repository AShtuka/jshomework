const  form = document.querySelector(".password-form");

const createWarning = () => {
    const spn = document.createElement("span");
    spn.innerText = "Нужно ввести одинаковые значения";
    spn.classList.add("spn");
    return spn;
}

const removeWarning = () => {
    if (document.querySelector(".spn") !== null) {
        document.querySelector(".spn").remove();
    }
}

const showPass = (item) => {
    if (item.className.includes("fa-eye-slash")) {
        item.previousElementSibling.setAttribute("type", "password");
        item.classList.remove("fa-eye-slash");
        item.classList.add("fa-eye");
    } else {
        item.previousElementSibling.setAttribute("type", "text");
        item.classList.remove("fa-eye");
        item.classList.add("fa-eye-slash");
    }
}

form.addEventListener("click", (event) => {
    event.preventDefault();
    if (event.target.className.includes("fas")) {
        showPass(event.target);
    } else if (event.target.className.includes("btn")) {
        const arrWithPass = document.querySelectorAll("input");
        if (arrWithPass[0].value === arrWithPass[1].value){
            alert("You are welcome");
        } else {
            removeWarning();
            event.target.before(createWarning());
        }
    } else {
        removeWarning();
    }
})