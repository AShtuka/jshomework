const askName = (message = "Input your name, please", name = "Ivan") => prompt(message, name);

//Simple version
const createNewUser = () => {
    const newUser = {};
    newUser.firstName = askName();
    newUser.lastName = askName("Input your surname, please", "Ivaniv");
    newUser.getLogin = function () {
        return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    }
    return newUser;
}

//Complicated version
const createNewUserComplicated = () => {
    const newUser = {};
    newUser.firstName = askName();
    newUser.lastName = askName("Input your surname, please", "Ivaniv");

        Object.defineProperty(newUser, "firstName", {
            writable: false,
            configurable: true
        });

        Object.defineProperty(newUser, "lastName", {
            writable: false,
            configurable: true
        });

        newUser.setFirstName = function() {
            Object.defineProperty(newUser, "firstName", {
                value: askName(),
            });
        }

        newUser.setLastName = function(){
            Object.defineProperty(newUser, "lastName", {
                value: askName("Input your surname, please", "Ivaniv"),
            });
        }

    newUser.getLogin = function () {
        return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    }

    return newUser;
}

// const user = createNewUserComplicated();
// const userSimple = createNewUser();
// console.log(user.getLogin());
// console.log(user);


