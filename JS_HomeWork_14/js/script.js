$(".tabs-content li").hide();
$("li[data-name='Akali']").show();

$(".tabs-title").click(function () {
    $(".tabs-title").removeClass("active");
    $(this).addClass("active");
    const tabName = $(this).attr("data-name");
    const tabForShow = `li[data-name="${tabName}"]`;
    $(".tabs-content li").hide();
    $(tabForShow).show();
})