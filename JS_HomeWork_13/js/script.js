let themeName = localStorage.getItem("theme");

const addDarkStyle = () => {
    const head = document.querySelector("head");
    const darkStyle = document.createElement("link");
    darkStyle.rel = "stylesheet";
    darkStyle.href = "css/darkStyle.css";
    darkStyle.id = "darkStyle";
    head.appendChild(darkStyle);
    localStorage.setItem("theme", "dark");
}

if (themeName) {
    if (themeName === "dark") {
        addDarkStyle();
    }
}

const ready = () => {
    const btn = document.querySelector(".nav-bar .text-block-link");
    btn.addEventListener("click", () => {
        if (!themeName){
            addDarkStyle();
        } else {
            themeName = localStorage.getItem("theme");
            if (themeName === "light") {
                addDarkStyle();
            } else {
                localStorage.setItem("theme", "light");
                document.getElementById("darkStyle").remove();
            }
        }
    })
}

document.addEventListener("DOMContentLoaded", ready);

