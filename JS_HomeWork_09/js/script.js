const tabs = document.querySelector(".tabs");

const changeActiveTab = (listTabs) => {
    for (let i = 0; i < listTabs.length; i++) {
        if(listTabs[i].className.includes("active")) {
            listTabs[i].classList.remove("active");
        }
    }
}

const replaceTabsContent = (tabName, tabsContent) => {
    for (let i = 0; i < tabsContent.length; i++) {
        if (tabsContent[i].dataset.name === tabName) {
            tabsContent[i].dataset.display = "true";
        } else {
            tabsContent[i].dataset.display = "false";
        }
    }
}

tabs.addEventListener("click", (event) => {
    const listTabs = event.currentTarget.children;
    changeActiveTab(listTabs);
    event.target.classList.add("active");
    const tabName = event.target.dataset.name;
    const tabsContent = document.querySelector(".tabs-content").children;
    replaceTabsContent(tabName, tabsContent);
})