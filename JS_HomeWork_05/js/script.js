const askName = (message = "Input your name, please", name = "Ivan") => prompt(message, name);

const askBirthday = (birthday = "dd.mm.yyyy") => {
    birthday = prompt("Input your birthday, please", birthday);
    const arr = birthday.split(".");
    const tmp = arr[0];
    arr[0] = arr[2];
    arr[2] = tmp;
    return arr;
}

const createNewUser = () => {
    const newUser = {};
    newUser.firstName = askName();
    newUser.lastName = askName("Input your surname, please", "Ivaniv");
    newUser.birthday = new Date(askBirthday());

    Object.defineProperty(newUser, "firstName", {
        writable: false,
        configurable: true
    });

    Object.defineProperty(newUser, "lastName", {
        writable: false,
        configurable: true
    });

    newUser.setFirstName = function() {
        Object.defineProperty(newUser, "firstName", {
            value: askName(),
        });
    }

    newUser.setLastName = function(){
        Object.defineProperty(newUser, "lastName", {
            value: askName("Input your surname, please", "Ivaniv"),
        });
    }

    newUser.getLogin = function () {
        return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    }

    newUser.getAge = function () {
        return ((new Date() - this.birthday) / 1000*60*60*24*365).toString().substr(0,2);
    }

    newUser.getPassword = function () {
        return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
    }

    return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());