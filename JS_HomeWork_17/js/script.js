function inputInteger(message) {
    let number = prompt(message);

    while(!(!isNaN(parseFloat(number)) && isFinite(number))) {
        number = prompt("Sorry, but this isn't number", number);
    }

    while (number - Math.round(number) != 0){
        number = prompt("Sorry but your number isn't integer", number);
        if(!(!isNaN(parseFloat(number)) && isFinite(number))) {
            number = prompt("Sorry, but this isn't number", number);
        }
    }

    return number
}

function numberFibonachiPlus(indexNumber,firstNum, secondNum) {
    if (indexNumber === 0) return 0;
    if (indexNumber === 1) return 1;
    if (indexNumber === 2) {
        return firstNum + secondNum;
    } else {
        return numberFibonachiPlus(indexNumber - 1, secondNum, firstNum + secondNum);
    }
}

function numberFibonachiMinus(indexNumber,firstNum, secondNum) {
    if (indexNumber === 0) return 0;
    if (indexNumber === -1) return 1;
    if (indexNumber === -2) {
        return firstNum - secondNum;
    } else {
        return numberFibonachiMinus(indexNumber + 1, secondNum, firstNum - secondNum);
    }
}

let firstNumber = +inputInteger("Hi, please enter your first number number");
let secondNumber = +inputInteger("Hi, please enter your second number");
let indexNumber = +inputInteger("Hi, please enter your index number");
if (firstNumber > secondNumber) {
    let temp = firstNumber;
    firstNumber = secondNumber;
    secondNumber = temp;
}

if (indexNumber > 0) {
    alert(numberFibonachiPlus(indexNumber, firstNumber, secondNumber));
} else {
    alert(numberFibonachiMinus(indexNumber, firstNumber, secondNumber));
}
